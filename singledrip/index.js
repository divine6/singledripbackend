const express = require("express")
const controller = require("./singledrip.controller")
const router = express.Router()


router.get("/",controller.getall)
router.get("/:dripid",controller.getById)
router.post('/',controller.create)
router.put('/:dripid',controller.updateDrip)
router.put('/:dripid/:status',controller.updateDripStatus)
router.delete('/:dripid',controller.deleteById)

module.exports=router
const mongoose = require("mongoose")

var SingledripCommonTemplate = new mongoose.Schema({
    dripCampaignName : String,
    stages:[{
        subject: String,
        body: String,
        stage: String,
        scheduleType : String,
        mode : String,
        afterCount : String,
        after : String
    }],
    isActive : {
        type:Boolean,
        default:false
    },
    modified_at : {
        type : Date,
        default : Date.now
    },
    createdAt : {
        type : Date,
        default : Date.now
    }
});

module.exports =  mongoose.model('SingledripCommonTemplate', SingledripCommonTemplate);

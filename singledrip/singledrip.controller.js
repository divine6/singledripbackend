const SingledripModel = require('./singledrip.model');

const getall = (req,res)=>{
    console.log("in getall")
    SingledripModel.find({},'dripCampaignName isActive',(err,result)=>{
        if(err){
            console.log("err ",JSON.stringify(err))
            res.send({status : "error"})
        }
        else{
            console.log("result ",result)
            res.send(result)
        }
    })
}
const getById = (req,res)=>{
    console.log("in getById")
    console.log("req.params ",req.params)
    let dripid = req.params.dripid
    SingledripModel.findById(dripid,(err,result)=>{
        if(err){
            console.log("err ",JSON.stringify(err))
            res.send({status : "error"})
        }
        else{
            console.log("result ",result)
            res.send(result)
        }
    })
}

const create = async (req,res)=>{
    console.log("in create")
    console.log("req.body ",req.body)
    let singledripModel = new SingledripModel(req.body);
    
    singledripModel.save((err,result)=>{
        if(err){
            console.log("err ",JSON.stringify(err))
            res.send({status : "error"})
        }
        else{
            console.log("result ",result)
            res.send({})
        }
    })
}

//update whole drip(including stages)
const updateDrip = (req,res)=>{
    console.log("in update")
    console.log("req.params " ,req.params)
    let dripid = req.params.dripid
    let reqbody = req.body;
    console.log("reqbody ",reqbody)
    SingledripModel.findOneAndUpdate({_id : dripid}, {...reqbody,modified_at:Date.now()}, (err,result)=>{
      if(err){
          console.log("err.message ",err.message)
          console.log("err.name ",err.name)

          res.send({status : "error"})
      }  
      else{
          console.log("result ",result)
          res.status(202).send({});
      }
    })
}

const updateDripStatus = (req,res)=>{
    console.log("in updateDripStatus")
    console.log("req.params " ,req.params)
    let dripid = req.params.dripid
    let status = req.params.status

    SingledripModel.update({_id: dripid}, {
        isActive: status,
        modified_at:Date.now()
    }, function(err, affected, result) {
        console.log("affected ",affected);
        if(err){
            console.log("err.message ",err.message)
            console.log("err.name ",err.name)
            res.send({status : "error"})
        }
        else{
            console.log("result ",result);
            res.status(202).send({});
        }
    })

}

//delete entire drip and not individual stages
const deleteById = (req,res)=>{
    console.log("in deleteById")
    console.log("req.params " ,req.params)
    let dripid = req.params.dripid
    SingledripModel.findOneAndDelete({_id:dripid},(err,result)=>{
        if(err){
            console.log("err.message ",err.message)
            console.log("err.name ",err.name)
            res.send({status : "error"})
        }
        else{
            console.log("result ",result);
            res.status(202).send({});
        }
    })
}
module.exports = {
    getall,getById,create,updateDrip,updateDripStatus,deleteById
}
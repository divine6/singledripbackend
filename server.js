const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const dbconfig = require('./dbconfig')
const express = require("express")
const cors = require("cors")
const app = express()
const PORT = 4900
mongoose.connect(dbconfig.mongo.uri, dbconfig.mongo.options);

mongoose.connection.on('connected', function() {
  console.error(`mongodb connected`);
});

mongoose.connection.on('error', function(err) {
  console.error(`MongoDB connection error: ${err}`);
  process.exit(-1); // eslint-disable-line no-process-exit
});

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())
app.use(cors())


app.get("/test",(req,res)=>{
    console.log("from /test")
    res.send({"status" : "from /test"})
})

/*
localhost:9000/api/adminpanel/singledrip
*/

app.use("/api/adminpanel/singledrip",require("./singledrip"))

app.listen(PORT,()=>{
    console.log("server listening on port ",PORT)
})



